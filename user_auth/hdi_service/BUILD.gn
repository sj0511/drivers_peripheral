# Copyright (c) 2022-2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//drivers/hdf_core/adapter/uhdf2/uhdf.gni")

ohos_shared_library("libuser_auth_interface_service_1.0") {
  include_dirs = [
    "adaptor/inc",
    "coauth/inc",
    "database/inc",
    "common/inc",
    "interface",
    "idm/inc",
    "key_mgr/inc",
    "user_auth/inc",
    "service",
    "//third_party/openssl/include",
    "../common/logs",
    "../common/utils",
  ]
  sources = [
    "adaptor/src/adaptor_algorithm.c",
    "adaptor/src/adaptor_file.c",
    "adaptor/src/adaptor_memory.c",
    "adaptor/src/adaptor_time.c",
    "adaptor/src/file_operator.c",
    "coauth/src/coauth.c",
    "coauth/src/coauth_funcs.c",
    "coauth/src/executor_message.c",
    "coauth/src/pool.c",
    "common/src/buffer.c",
    "common/src/linked_list.c",
    "common/src/tlv_base.c",
    "common/src/tlv_wrapper.c",
    "database/src/idm_common.c",
    "database/src/idm_database.c",
    "database/src/idm_file_manager.c",
    "hal_sdk/useriam_common.cpp",
    "idm/src/enroll_specification_check.c",
    "idm/src/idm_session.c",
    "idm/src/user_idm_funcs.c",
    "key_mgr/src/ed25519_key.c",
    "key_mgr/src/token_key.c",
    "service/user_auth_interface_service.cpp",
    "user_auth/src/auth_level.c",
    "user_auth/src/context_manager.c",
    "user_auth/src/identify_funcs.c",
    "user_auth/src/user_auth_funcs.c",
    "user_auth/src/user_sign_centre.c",
  ]
  external_deps = [
    "c_utils:utils",
    "drivers_interface_user_auth:user_auth_idl_headers",
    "hilog_native:libhilog",
    "hiviewdfx_hilog_native:libhilog",
    "ipc:ipc_single",
  ]
  deps = [ "//third_party/openssl:libcrypto_shared" ]

  remove_configs = [ "//build/config/compiler:no_exceptions" ]

  install_images = [ chipset_base_dir ]
  subsystem_name = "hdf"
  part_name = "drivers_peripheral_user_auth"
}

ohos_shared_library("libuser_auth_driver") {
  include_dirs = [
    "interface",
    "../common/logs",
    "../common/utils",
  ]
  sources = [ "service/user_auth_interface_driver.cpp" ]
  deps = [ "../../../interface/user_auth/v1_0:libuser_auth_stub_1.0" ]

  external_deps = [
    "c_utils:utils",
    "hdf_core:libhdf_host",
    "hdf_core:libhdf_ipc_adapter",
    "hdf_core:libhdf_utils",
    "hdf_core:libhdi",
    "hilog_native:libhilog",
    "hiviewdfx_hilog_native:libhilog",
    "ipc:ipc_single",
  ]

  remove_configs = [ "//build/config/compiler:no_exceptions" ]

  shlib_type = "hdi"
  install_images = [ chipset_base_dir ]
  subsystem_name = "hdf"
  part_name = "drivers_peripheral_user_auth"
}

group("hdf_user_auth_service") {
  deps = [
    ":libuser_auth_driver",
    ":libuser_auth_interface_service_1.0",
  ]
}
